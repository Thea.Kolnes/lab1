package rockPaperScissors;

import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.Scanner;

public class RockPaperScissors {
	
	public static void main(String[] args) {
    	/* 	
    	 * The code here does two things:
    	 * It first creates a new RockPaperScissors -object with the
    	 * code `new RockPaperScissors()`. Then it calls the `run()`
    	 * method on the newly created object.
         */
        new RockPaperScissors().run();
    }
    
    
    Scanner sc = new Scanner(System.in);
    int roundCounter = 1;
    int humanScore = 0;
    int computerScore = 0;
    List<String> rpsChoices = Arrays.asList("rock", "paper", "scissors");
    
    public void run() {
        
        while (true) {
            System.out.println("Let's play round " + roundCounter);
            String humanChoice = userChoice();
            String computerChoice = rpsChoice();
            String choiceString = "Human chose " + humanChoice + ", computer chose " + computerChoice +".";
    
            if (isWinner(humanChoice, computerChoice)) {
                System.out.println(choiceString + " Human wins.");
                humanScore++;
            } else if (isWinner(computerChoice, humanChoice)) {
                System.out.println(choiceString + " Computer wins.");
                computerScore++;
            } else {
                System.out.println(choiceString + " It's a tie");
            }
            System.out.println("Score: human " + humanScore + " computer " + computerScore);
            
            if (continuePlaying().equals("n")) {
                break;
            } else {
                roundCounter++;
                continue;
            }
        }
        System.out.println("Bye bye :)");
        }

    // method is a function that can return to main class/method
    public String rpsChoice() {
        Random random = new Random(); // how to choose random from array
        int randomChoice = random.nextInt(rpsChoices.size());
        String randomRpsChoice = rpsChoices.get(randomChoice);
        return randomRpsChoice;
    }
    public boolean isWinner(String choice1, String choice2) {
        if (choice1.equals("paper")) {
            return choice2.equals("rock");
        } else if (choice1.equals("scissors")) {
            return choice2.equals("paper");
        } else {
            return choice2.equals("scissors");
        }
    }
    public String userChoice() {
        while (true) {
            String humanChoice = readInput("Your choice (Rock/Paper/Scissors)?").toLowerCase();
            if (rpsChoices.contains(humanChoice)) {
                return humanChoice;
            } else {
                System.out.println("I don't understand " + humanChoice +  ". Try again");
            }
        }
    }   
    public String continuePlaying() {
        List<String> validAnswer = Arrays.asList("y", "n");
        while (true) {
            String continueAnswer = readInput("Do you wish to continue playing? (y/n)?").toLowerCase();
            if (validAnswer.contains(continueAnswer)) {
                return continueAnswer;
            } else {
                System.out.println("I don't understand " + continueAnswer + ". Try again");
                continue;
            }
        }
    }

    
    

    /**
     * Reads input from console with given prompt
     * @param prompt
     * @return string input answer from user
     */
    public String readInput(String prompt) {
        System.out.println(prompt);
        String userInput = sc.next();
        return userInput;
    }

}
